// File:          super.cpp
// Date:          
// Description:   
// Author:        
// Modifications: 

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <webots/Robot.hpp>
#include <webots/Supervisor.hpp>

#include "ros/ros.h"
#include "std_msgs/String.h"

using namespace webots;
using namespace std;

class super : public Supervisor {
    ros::Publisher imagePub;
    ros::Publisher alignPub;
    ros::Publisher graspPub;
    ros::Publisher boxLiftedPub;
    ros::Publisher boxPlacedDownPub;
    ros::Publisher boxDroppedFloor;
    ros::Publisher killRosbagRecording;

    ros::Subscriber refreshWebots;

    ros::NodeHandle n;
    std_msgs::String msg;

    Node *Nao;
    Field *naoTranslation;
    const double *naoVec;

    Node *Boxes[2];
    Field *boxTranslation[2];
    const double *boxVec;

    double timeStep;

    bool boxLifted[2];
    bool boxPlacedOnTable[2];
    bool boxFallen[2];
    bool endThreads;
    bool rosbagKilled;

    bool okToRun;
    int counter;
    int myTimer;
    string path;
    string home;
    string filename;

    double timestamp;

    float xMinAlign;
    float xMaxAlign;

    float xMinGrab;
    float xMaxGrab;

    float xMinDrop;
    float xMaxDrop;

    typedef map<string, float> Coords;

    Coords boxAlign0;
    Coords boxAlign1;
    Coords boxGrab0;
    Coords boxGrab1;
    Coords boxDrop0;
    Coords boxDrop1;
    Coords boxAlignVals[2];
    Coords boxGrabVals[2];
    Coords boxDropVals[2];
    stringstream ss;
    ostringstream ostr;

    public:

    super() : Supervisor() {
        imagePub = n.advertise<std_msgs::String>("webots_image", 10);
        alignPub = n.advertise<std_msgs::String>("aligning", 10);
        graspPub = n.advertise<std_msgs::String>("grasping", 10);
        boxLiftedPub = n.advertise<std_msgs::String>("box_lifted", 10);
        boxPlacedDownPub = n.advertise<std_msgs::String>("box_placed_down", 10);
        boxDroppedFloor = n.advertise<std_msgs::String>("box_dropped", 10);
        killRosbagRecording = n.advertise<std_msgs::String>("kill_rosbag", 10);

        refreshWebots = n.subscribe("refresh_webots", 10, &super::refresh_webots_callback, this);

        boxAlign0["zMin"] = -2.2;
        boxAlign0["zMax"] = -0.95;
        boxAlign1["zMin"] = -0.95;
        boxAlign1["zMax"] = 0.3;
        boxAlignVals[0] = boxAlign0;
        boxAlignVals[1] = boxAlign1;

        boxGrab0["zMin"] = -1.76;
        boxGrab0["zMax"] = -1.36;
        boxGrab1["zMin"] = -0.51;
        boxGrab1["zMax"] = -0.11;
        boxGrabVals[0] = boxGrab0;
        boxGrabVals[1] = boxGrab1;

        boxDrop0["zMin"] = -1.95;
        boxDrop0["zMax"] = -1.2;
        boxDrop1["zMin"] = -0.7;
        boxDrop1["zMax"] = -0.04;
        boxDropVals[0] = boxDrop0;
        boxDropVals[1] = boxDrop1;

        xMinAlign = 4.1;
        xMaxAlign = 4.66;

        xMinGrab = 4.33;
        xMaxGrab = 4.53;

        xMinDrop = 3.12;
        xMaxDrop = 3.4;

        rosbagKilled = false;

        timeStep = getBasicTimeStep();
     
        Nao = getFromDef("NAO");
        if (Nao != 0){
            naoTranslation = Nao->getField("translation");
            if (naoTranslation != 0){
                okToRun = true;
            }
        }

        counter = 0;
        home = getenv("HOME");
        path = home + "/workspace/src/userplancs/scripts/WUI_userplancs/webotsImages/";
      
        if (okToRun == true){
            for (int i = 0; i < 2; i++){
                ostringstream ostr;
                ostr << i;
                string boxName = "BOX" + ostr.str();
                Boxes[i] = getFromDef(boxName);
                if (Boxes[i] != 0){
                    boxTranslation[i] = Boxes[i]->getField("translation");
                    if (boxTranslation[i] != 0){
                      okToRun = true;
                    } else {
                      okToRun = false;
                      break;
                    }
                } else {
                    okToRun = false;
                    break;
                }
            }
        }
    }

    void refresh_webots_callback(const std_msgs::String::ConstPtr& msg)
    {
        if (msg->data == "refresh"){
            simulationRevert();
        }
    }

    void CheckKillRosbag(){
        bool killBag = false;
        if (boxPlacedOnTable[0] == true && boxPlacedOnTable[1] == true){
            killBag = true;
        }
        else if (boxPlacedOnTable[0] && boxFallen[1]){
            killBag = true;
        }
        else if (boxPlacedOnTable[1] && boxFallen[0]){
            killBag = true;
        }
        else if (boxFallen[0] && boxFallen[1]){
            killBag = true;
            //
        }
        if (rosbagKilled == false && killBag == true){
            cout << "Killing rosbag recording now." << endl;
            ss.str("");
            ss << "kill";
            msg.data = ss.str();
            killRosbagRecording.publish(msg);
            rosbagKilled = true;
        }
    }

    // super destructor
    virtual ~super() {
      
      // Enter here exit cleanup code
    }
    
    void run() {
        // Main loop:
        if (okToRun == true){
            ros::Rate loop_rate(60);
            timestamp = current_timestamp();
            myTimer = 0;
            while (step((int)timeStep) != -1) {
                if (timestamp < current_timestamp() - 200){
                    ostr.str("");
                    ostr << counter;
                    filename = "image" + ostr.str() + ".jpg";

                    ss.str("");
                    ss << filename;
                    msg.data = ss.str();
                    exportImage(path + filename, 10);
                    imagePub.publish(msg);

                    timestamp = current_timestamp();
                    counter++;
                    if (counter == 5){
                        counter = 0;
                    }
                }
                naoVec = naoTranslation->getSFVec3f();
                
                BoxMovements();
                if (naoVec[2] > boxAlignVals[0]["zMin"] && naoVec[2] < boxAlignVals[0]["zMax"] && boxLifted[0] == false){
                    Align(0);
                    Grasp(0);
                }
                else if (naoVec[2] > boxAlignVals[1]["zMin"] && naoVec[2] < boxAlignVals[1]["zMax"] && boxLifted[1] == false){
                    Align(1);
                    Grasp(1);
                }
/*
                if (myTimer < 300)
                    myTimer += 1;
                else if(myTimer == 300){
                    cout << "Launching all ROS scripts now..." << endl;
                    cout << "--------------------------------" << endl;

                    char buffer[512];
                    FILE* file;
                    if (!(file = popen("roslaunch userplancs all_launch.launch", "r"))){
                        while (fgets(buffer, sizeof(buffer), file)!= NULL){
                            cout << buffer <<endl;
                        }
                        pclose(file);
                    }
                    myTimer += 1;
                }
*/
            
                ros::spinOnce();
                loop_rate.sleep();
                CheckKillRosbag();
                timeStep = getBasicTimeStep();
            }
        }
    }

    long long current_timestamp() {
        struct timeval te; 
        gettimeofday(&te, NULL);
        long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000;
        return milliseconds;
    }

    void BoxMovements(){
        for (int i = 0; i < 2; i++){
            boxVec = boxTranslation[i]->getSFVec3f();

            // Publish a message if dropped on floor 
            if (boxFallen[i] == false){
                if (boxVec[1] < 0.1){
                    stringstream ss;
                    ss << i;
                    msg.data = ss.str();
                    boxDroppedFloor.publish(msg);
                    //cout << "Box Dropped " << i << endl;
                    boxFallen[i] = true;
                }
            }

            // If dropped back on table 
            if (boxVec[1] > 0.41 && boxVec[1] < 0.45 && boxVec[0] < 4.7 && boxVec[0] > 4.5){
                boxLifted[i] = false;
            }

            // Publish a message if box is placed on table.
            if (boxPlacedOnTable[i] == false){
                if (boxVec[2] > boxDropVals[i]["zMin"] && boxVec[2] < boxDropVals[i]["zMax"]){
                    if (boxVec[0] > xMinDrop && boxVec[0] < xMaxDrop){
                        if (boxVec[1] > 0.37 && boxVec[1] < 0.41){
                            boxPlacedOnTable[i] = true;
                            //cout << "On Table " << i << endl;
                            stringstream ss;
                            ss << i;
                            msg.data = ss.str();
                            boxPlacedDownPub.publish(msg);
                        }
                    }
                }
            }
        }
    }

    void Align(int i){
        if (naoVec[0] > xMinAlign && naoVec[0] < xMaxAlign){
            if (naoVec[0] < xMinGrab || naoVec[2] < boxGrabVals[i]["zMin"] || naoVec[2] > boxGrabVals[i]["zMax"]){
                //cout << "Aligning " << i << endl;
                stringstream ss;
                ss << i;
                msg.data = ss.str();
                alignPub.publish(msg);
            }
        }
    }

    void Grasp(int i){
        if (naoVec[0] > xMinGrab && naoVec[0] < xMaxGrab){
            if (naoVec[2] > boxGrabVals[i]["zMin"] && naoVec[2] < boxGrabVals[i]["zMax"]){
                BoxLifted(i);
                if (boxLifted[i] == false){
                    stringstream ss;
                    ss << i;
                    msg.data = ss.str();
                    graspPub.publish(msg);
                    //cout << "Grasping " << i << endl;
                }
            }
        }
    }

    void BoxLifted(int index){
        boxVec = boxTranslation[index]->getSFVec3f();
        if (boxVec[1] > 0.45){
            //cout << "Lifted " << index << endl;
            boxLifted[index] = true;
            stringstream ss;
            ss << index;
            msg.data = ss.str();
            boxLiftedPub.publish(msg);
        }
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "supervisor_control");
    super* controller = new super();
    controller->run();
    delete controller;
    return 0;
}
