#!/usr/bin/env python
from std_msgs.msg import *
from subprocess import *
from controller import Robot
from controller import Supervisor
from plancs import *
import plancs, time, rospy, os

class mySupervisor (Supervisor):

    xMinAlign = 4.1
    xMaxAlign = 4.66

    xMinGrab = 4.33
    xMaxGrab = 4.53

    xMinDrop = 3.12
    xMaxDrop = 3.4

    boxAlign0 = {'zMin': -2.2, 'zMax': -0.95}
    boxAlign1 = {'zMin': -0.95, 'zMax': 0.3}
    boxAlignVals = [boxAlign0, boxAlign1]

    boxGrab0 = {'zMin': -1.76, 'zMax': -1.36}
    boxGrab1 = {'zMin': -0.51, 'zMax': -0.11}
    boxGrabVals = [boxGrab0, boxGrab1]

    boxDrop0 = {'zMin': -1.95, 'zMax': -1.2}
    boxDrop1 = {'zMin': -0.7, 'zMax': -0.04}
    boxDropVals = [boxDrop0, boxDrop1]

    boxValues = []

    mynode = plancs.Plancs_Node("supervisor_node")
    alignPub = rospy.Publisher("aligning", String)
    graspPub = rospy.Publisher("grasping", String)
    boxLiftedPub = rospy.Publisher("box_lifted", String)
    boxPlacedDownPub = rospy.Publisher("box_placed_down", String)
    boxDroppedFloor = rospy.Publisher("box_dropped", String)

    def RefreshWebots(self, msg):
        if msg.data == "refresh":
            self.simulationRevert()

    def run(self):
        self.mynode.add_subscriber("refresh_webots", String, self.RefreshWebots)
        timestep = int(self.getBasicTimeStep())
        okToRun = False
        timestamp = time.time()
        counter = 0
        timer = 0
        imagePub = rospy.Publisher("webots_image", String)

        # Nao coordinates
        nao = self.getFromDef("NAO")
        translation = None
        if nao != None:
            translation = nao.getField("translation")
            if translation != None:
                okToRun = True
            else:
                okToRun = False
        else: 
            okToRun = False
        

        # boxValues = [boxObject, boxTranslation, ifLifted, ifPlacedOnTable, ifFallenOnFloor]
        if okToRun == True:
            for i in range(2):
                self.boxValues.append([])
                boxName = "BOX" + str(i)
                self.boxValues[i].append(self.getFromDef(boxName))
                if self.boxValues[i][0] != None:
                    self.boxValues[i].append(self.boxValues[i][0].getField("translation"))
                    self.boxValues[i].append(False)
                    self.boxValues[i].append(False)
                    self.boxValues[i].append(False)
                    okToRun = True
                else:
                    okToRun = False
                    break

        if okToRun == True:
            for i in range(2):
                if self.boxValues[i][1] != None:
                    okToRun = True
                else:
                    okToRun = False
                    break

        if okToRun == True:
            while self.step(timestep) != -1:
                if timestamp < time.time() - 0.2:
                    path = "../../scripts/WUI_userplancs/webotsImages/"
                    filename = "image" + str(counter) + ".jpg"
                    self.exportImage(path + filename, 10)
                    timestamp = time.time()
                    imagePub.publish(str(filename))
                    counter += 1
                    if counter == 5:
                        counter = 0

                naoLoc = translation.getSFVec3f()
                if naoLoc[2] > self.boxAlign0['zMin'] and naoLoc[2] < self.boxAlign0['zMax'] and self.boxValues[0][2] == False:
                    self.Align(naoLoc, 0)
                    self.Grasp(naoLoc, 0)
                    self.BoxMovements(0)
                        
                elif naoLoc[2] > self.boxAlign1['zMin'] and naoLoc[2] < self.boxAlign1['zMax'] and self.boxValues[1][2] == False:
                    self.Align(naoLoc, 1)
                    self.Grasp(naoLoc, 1)
                    self.BoxMovements(1)
                                

                rospy.sleep(0.0001)

                if timer < 300:
                    timer += 1
                elif timer == 300:
                    print "Launching all ROS scripts now..."
                    print "--------------------------------"
                    command = "roslaunch userplancs all_launch.launch"
                    launchAll = Popen(command, stdin=PIPE, shell=True)
                    #call("roslaunch userplancs all_launch.launch", shell=True)
                    #pid = Popen(args=["gnome-terminal", "--command=roslaunch userplancs all_launch.launch"]).pid
                    #print pid
                    timer += 1

        else:
            self.ErrorNames()

    def BoxMovements(self, index):
        boxLoc = self.boxValues[index][1].getSFVec3f()
        if self.boxValues[index][4] == False:
            if boxLoc[1] < 0.1:
                self.boxDroppedFloor.publish(str(index))
                #print "Box Dropped"
                self.boxValues[index][4] = True


        # If dropped back on table    
        if boxLoc[1] > 0.41 and boxLoc[1] < 0.45 and boxLoc[0] < 4.7 and boxLoc[0] > 4.5:
            self.boxValues[index][2] = False

        # Publish a message if box is placed on table.    
        if self.boxValues[index][3] == False:
            if boxLoc[2] > self.boxDropVals[index]['zMin'] and boxLoc[2] < self.boxDropVals[index]['zMax']:
                if boxLoc[0] > self.xMinDrop and boxLoc[0] < self.xMaxDrop:
                    if boxLoc[1] > 0.37 and boxLoc[1] < 0.41:
                        self.boxValues[index][3] = True
                        #print "On Table " + str(i)
                        self.boxPlacedDownPub.publish(str(index))



    def Align(self, naoLoc, index):
        if naoLoc[0] > self.xMinAlign and naoLoc[0] < self.xMaxAlign:
            if naoLoc[0] < self.xMinGrab or naoLoc[2] < self.boxGrabVals[index]['zMin'] or naoLoc[2] > self.boxGrabVals[index]['zMax']:
                #print "Aligning " + str(index)
                self.alignPub.publish(str(index))

    def Grasp(self, naoLoc, index):
        if naoLoc[0] > self.xMinGrab and naoLoc[0] < self.xMaxGrab:
            if naoLoc[2] > self.boxGrabVals[index]['zMin'] and naoLoc[2] < self.boxGrabVals[index]['zMax']:
                self.BoxLifted(index)
                if self.boxValues[index][2] == False:
                    self.graspPub.publish(str(index))
                    #print "Grasping " + str(index)


    def BoxLifted(self, index):
        boxLoc = self.boxValues[index][1].getSFVec3f() 
        if boxLoc[1] > 0.45:
            #print "Lifted"
            self.boxLiftedPub.publish(str(index))
            self.boxValues[index][2] = True

    def ErrorNames(self):
        print "The Nao needs to be defined as: NAO"
        print "The boxes need to be defined as: BOX0 and BOX1"

controller = mySupervisor()
controller.run()