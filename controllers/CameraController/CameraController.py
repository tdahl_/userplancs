
from controller import Robot
from controller import Camera
import cv, sys, os, rospy, cv2, time
from cv_bridge import CvBridge, CvBridgeError
import plancs
from plancs import *
from cv import IPL_DEPTH_8U
import numpy as np
from sensor_msgs.msg import Image
from std_msgs.msg import String
from rospy.numpy_msg import numpy_msg

class CameraController (Robot):

    def run(self):
        bridge = CvBridge()
        dtype, n_channels = bridge.encoding_as_cvtype2('bgr8')

        name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
        argv = rospy.myargv(argv=sys.argv)

        mynode = plancs.Plancs_Node(name)


        timestep = int(self.getBasicTimeStep())

        camera = self.getCamera("myCamera")
        camera.enable(2 * timestep)

        camPub = rospy.Publisher("webots_image", String)
        timestamp = time.time()
        counter = 

        while True:
            if timestamp < time.time() - 0.4:
                path = "../../scripts/WUI_userplancs/webotsImages/"
                filename = "image" + str(counter) + ".jpg"
                camera.saveImage(path + filename, 10)
                timestamp = time.time()
                camPub.publish(str(filename))
                counter += 1
                if counter == 5:
                    counter = 0
#            cameraData = camera.getImageArray()
#
 #           cameraData = np.asarray(cameraData, dtype=dtype)#.reshape(width, height, 3)
  #          cameraData = np.fliplr(cameraData)
   #         cameraData = np.rot90(cameraData)
    #        image_message = bridge.cv2_to_imgmsg(cameraData, encoding="rgb8")
     #       camPub.publish(image_message)
            #print 1
            rospy.sleep(0.00001)
            if self.step(timestep) == -1:
                break

controller = CameraController()
controller.run()
