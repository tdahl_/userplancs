#!/usr/bin/env python

import os, sys, rospy, plancs
from plancs import *
from naoqi import ALProxy
from userplancs.msg import Sonar

#sonarProxy = PALProxy("ALSonar")

# Gets the sonar readings from the Nao and publishes the values on a ROS message.
def get_sonar():

	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
	argv = rospy.myargv(argv=sys.argv)
	if len(argv) >=2:
		name = name + "_" + argv[1]

	sonarProxy = None
	memoryProxy = None

	try:
		sonarProxy = ALProxy("ALSonar", "127.0.0.1", 9559)
		memoryProxy = ALProxy("ALMemory", "127.0.0.1", 9559)
	except RuntimeError, e:
		rospy.logerr("Could not create Proxy to ALMotion or ALMemory, exiting. \nException message:\n%s", e)
		exit(1)

	sonarData = Sonar()
	mynode = plancs.Plancs_Node(name)
	sonarPub = rospy.Publisher("sonar_values", Sonar)

	sonarProxy.subscribe("myApplication")

	while not rospy.is_shutdown(): 

		# Get sonar left first echo (distance in meters to the first obstacle).
		sonarData.left = memoryProxy.getData("Device/SubDeviceList/US/Left/Sensor/Value")

		# Same thing for right.
		sonarData.right = memoryProxy.getData("Device/SubDeviceList/US/Right/Sensor/Value")

		if sonarData.right <= 2.5 and sonarData.right > 0.25 or sonarData.left <= 2.5 and sonarData.left > 0.25:
			sonarPub.publish(sonarData)

		rospy.sleep(0.5)

if __name__ == '__main__':
    get_sonar()
