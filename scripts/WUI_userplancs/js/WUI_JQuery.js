    
// Dynamically adds a JQuery range slider to the page. It requires a text box to be a slave, which it overwrites. 
function SpawnRangeSliders(slider){
	var sliderDiv = document.getElementById("divSliderContainer");
	var para = document.createElement("p");
	para.setAttribute("id", "par" + slider.postFix);
	para.setAttribute("class", "parSliderTitle");
    sliderDiv.appendChild(para);
    document.getElementById("par" + slider.postFix).innerText = slider.postFix;
	var textBox = document.createElement("input");
	textBox.setAttribute("type", "textBox");
    textBox.setAttribute("value", "0");
    textBox.setAttribute("id", slider.rangeID);
    sliderDiv.appendChild(textBox);

	$("#" + slider.rangeID).ionRangeSlider({
		min 	: slider.minimum, 
		max 	: slider.maximum, 
		from 	: slider.fromNo,
		to 		: slider.toNo,
		type 	: 'double',
		step 	: slider.stepSize,
		postfix : slider.postFix,
		grid 	: true,
		grid_num: 10,
		disable : true,
		onFinish: function (obj) {
			slider.fromNo = obj.fromNumber;
			slider.toNo = obj.toNumber;
		}
	});

	var element1 = document.createElement('hr');
    sliderDiv.appendChild(element1);
}

// Removes all range sliders from the display. Because this method completely removes everything
// from within the div, a call is made to 'PopulateGreenStripe()' and 'PopulateBlueStripe()'
// which returns these elements to the display if they are meant to be there.
function RemoveRangeSliders() {
	if (editingNode === true)
	{
		if (typeof userSenseNodes[userSenseNodes.length - 1].RangeSlidersList !== 'undefined' 
		&& userSenseNodes[userSenseNodes.length - 1].RangeSlidersList.length > 0) {
		
			for (var i = 0; i < userSenseNodes[userSenseNodes.length - 1].RangeSlidersList.length; i++){
				//var elem = document.getElementById(userSenseNodes[userSenseNodes.length - 1].RangeSlidersList[i].rangeID);
				//elem.parentNode.removeChild(elem);
				document.getElementById("divSliderContainer").innerHTML = "";
				$("#" + userSenseNodes[userSenseNodes.length - 1].RangeSlidersList[i].rangeID).ionRangeSlider("remove");				
			}
		}
	}
	else if (makeNewNode === true)
	{
		if (typeof tempSlidersList[tempSlidersList.length - 1] !== 'undefined' 
			&& tempSlidersList.length > 0) {
			
			for (var i = 0; i < tempSlidersList.length; i++){
				//var elem = document.getElementById(userSenseNodes[userSenseNodes.length - 1].RangeSlidersList[i].rangeID);
				//elem.parentNode.removeChild(elem);
				document.getElementById("divSliderContainer").innerHTML = "";
				$("#" + tempSlidersList[tempSlidersList.length - 1].rangeID).ionRangeSlider("remove");
			}
		}
	}

	var greenStripe = document.getElementById("plancs_green_stripe");
	if (greenStripe){
		if (greenStripe.checked){
			PopulateGreenStripe();
		}
	}

	var blueStripe = document.getElementById("plancs_blue_stripe");
	if (blueStripe){
		if (blueStripe.checked){
			PopulateBlueStripe();
		}
	}
}