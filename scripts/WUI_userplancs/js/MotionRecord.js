

var jointDataArray = [];
var jointNameArray = [];
var poseArray = [];
var poseCount = 0;
var motionLength = 0;
var theMotionFile;
var motionTimer;
var motionRecordPageUsed = true;
var tempMotionNode = "";


// Recording starts here. All visual elements dealt with.
function RecordMotion(){
	TurnLEDON("imgRecordingStatus");
	btnDisableTraining.disabled = true;
	btnRecordMotion.style.display = "none";
	btnStopRecordMotion.style.display = "inline";
	btnReplayMotion.style.display = "none";
	btnConnectMotion.style.display = "none";
	GetJointNames();
	allowEditUserMotionNodes = false;
	AllowEditUserMotionNodes();
}

// Recording stops here. All visual elements dealt with.
function StopMotionRecord(){
	btnRecordMotion.style.display = "inline";
	btnStopRecordMotion.style.display="none";
	btnReplayMotion.style.display = "inline";
	btnConnectMotion.style.display = "inline";
	allowEditUserMotionNodes = false;
	AllowEditUserMotionNodes();
	parMotionFile.innerHTML = "Motion File Finished";
	TurnLEDOFF("imgRecordingStatus");
	btnDisableTraining.disabled = false;
}

// Starting the recording
function StartMotionRecorder(edit){
	if (edit == false){
		divMotionRecorder.style.display = "inline";
		allowEditUserMotionNodes = false;
		AllowEditUserMotionNodes();
	}
}

// An override of the WUI_JS file. Some elements don't exist in some pages.
function AllowEditUserMotionNodes(){
	if (allowEditUserMotionNodes == true){
		btnNewMotion.disabled = false;
		btnEditMotion.disabled = false;
		btnDeleteMotion.disabled = false;
		if (systemMotionNodes.length > 0)
			btnNewMotion.disabled = true;
	}
	else{
		btnNewMotion.disabled = true;
		btnEditMotion.disabled = true;
		btnDeleteMotion.disabled = true;
	}
}


// Populating the list of joint names for use in the motion file. This is hardcoded - we need access to the head and both arms. 
// Indexes 0-7 and 20-25
function GetJointNames(){

	jointNameArray = [];
		
	var initjointData = new ROSLIB.Topic({
		ros : ros,
		name : 'joint_states',
		messageType : 'sensor_msgs/JointState'
	});

	initjointData.subscribe(function(message) {

		var i = 0;

		while (i < 25){
			jointNameArray.push(message.name[i]);

			i++;
			if (i == 7) 
				i = 20;		
			
		}
		initjointData.unsubscribe();
		InitialiseMotionFile();
		motionTimer = setInterval("GetJointData()",1000);
	});
}

// Accessing the same indexes from the joint names, capturing and storing the joint angles from the given names.
function GetJointData(){

	var jointData = new ROSLIB.Topic({
		ros : ros,
		name : 'joint_states',
		messageType : 'sensor_msgs/JointState'
	});

	jointData.subscribe(function(message) {
		var i = 0;

		jointDataArray = [];

		while (i < 25){
			jointDataArray.push(message.position[i].toFixed(3));
			i++;
			if (i == 7) 
				i = 20;
		}
		jointData.unsubscribe();
		
		poseArray[poseCount] = jointDataArray;
		PopulateMotionFile();
	});

	parMotionFile.innerHTML = poseCount + "/60 Seconds Max.";
}

// Printing the initial header and populating the rest of the first line. Adds commas between each joint name, except the last. 
function InitialiseMotionFile(){
	theMotionFile = "#WEBOTS_MOTION,V1.0,";

	for (var i = 0; i < 12; i++){
		theMotionFile += jointNameArray[i];
		if (i !== 11)
			theMotionFile += ","
	}
	theMotionFile += "\n";
}


// Populates each line of the motion file, adding a timestep and all the joint angles. Each step increments a second, up to 60 max.
function PopulateMotionFile(){

	if (poseCount < 10){
		theMotionFile += "00:0" + poseCount + ":000,Pose" + poseCount + ",";
		AddJointData();
	}
	else if (poseCount < 60){
		theMotionFile += "00:" + poseCount + ":000,Pose" + poseCount + ",";
		AddJointData();
	}
	else if (poseCount == 60){
		theMotionFile += "01:00:000,Pose" + poseCount + ",";
		AddJointData();
		FinishMotionFileAndSend();
		allowEditUserMotionNodes = true;
		AllowEditUserMotionNodes();
		btnEditMotion.disabled = true;
		TurnLEDOFF("imgRecordingStatus");
	}
}

// Adding previously captured array joint data into the motion file. The motion file itself is one big string.
function AddJointData(){
	for (var i = 0; i < 12; i++){
		theMotionFile += jointDataArray[i];
		if (i !== 11)
			theMotionFile += ",";
	}
	theMotionFile += "\n";
	if (poseCount !== 60)
		poseCount++;
}


// Clearing intervals and calling the service to send the motion file. Also filling a system sense node object so it can be 
// easily displayed.
function FinishMotionFileAndSend(){
	clearInterval(motionTimer);
	motionLength = poseCount;
	poseCount = 0;

	var createMotion = new ROSLIB.Service({
		ros : ros,
		name : '/create_motion',
		serviceType: 'userplancs/UserplancsString'
	})
	var requester = new ROSLIB.ServiceRequest({
		request : theMotionFile
	});

	createMotion.callService(requester, function(result){});

	tempMotionNode = new systemMotionNode();
	tempMotionNode.Name = "userMotion0"; // + systemMotionNodes.length;
	tempMotionNode.DisplayName = "My Motion"; // + systemMotionNodes.length;
	tempMotionNode.ParagraphForFlashingBox = "parSystemMotionStatus0"; // + systemMotionNodes.length;

	document.getElementById("divSystemMotionNodes").innerHTML = "";
	document.getElementById("divSystemMotionStatus").innerHTML = "";
	
	StopMotionRecord();
}

// A call to play the motion on request of the user. 
function ReplayMotion(){
	var grabMotion = new ROSLIB.Service({
        ros : ros,
        name : '/user_motion',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({});

    grabMotion.callService(requester, function(result){});
}

// Connecting a sensor to the motion. Not much happens here - the checks are made elsewhere.
function ConnectMotion(){
	nodeConnected = true;
	systemMotionNodes = [];
	document.getElementById("divSystemMotionNodes").innerHTML = "";
	document.getElementById("divSystemMotionStatus").innerHTML = "";
	systemMotionNodes.push(tempMotionNode);
	TrackSystemMotionConnections();
	alert("Motion connected!");
	divMotionRecorder.style.display = "none";
	btnEditMotion.disabled = false;
}

// Visual updates when a motion is deleted. Also publishes a message for the ROSbag recorder.
function DeleteMotion(){
	var index = GetSelectedMotionIndex();

	if (index !== -1)
	{
		nodeConnected = false;
		document.getElementById("divSystemMotionNodes").innerHTML = "";
		document.getElementById("divSystemMotionStatus").innerHTML = "";
		systemMotionNodes = [];
		parMotionFile.innerText = "";
		btnReplayMotion.style.display = "none";
		btnConnectMotion.style.display = "none";
		divMotionRecorder.style.display = "none";
		allowEditUserMotionNodes = true;
		AllowEditUserMotionNodes();

		// Publish a message when the motion is deleted.
		var deleteMotion = new ROSLIB.Topic({
			ros : ros,
			name : '/motion_deleted',
			messageType : 'std_msgs/String'
	    });
	    var empty_message = new ROSLIB.Message({
	    	data: ""
	    });
	    deleteMotion.publish(empty_message);
	    // ---------------------------------------------
	}
	else
		alert("Please select your motion skill.");
}

// Checking if the motion is selected and displaying HTML.
function EditMotion(){
	divMotionRecorder.style.display = "inline";
}