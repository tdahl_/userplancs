
var ros;

// A timer is set which repeatedly calls 'RosConnection()'
rosConnector = setInterval(function(){RosConnection()}, 500);

// A function which checks to see if a connection can be made to
// ROS. Once a connection is made then 'CheckFullROSStartup()' is called.
function RosConnection(){
    ros = new ROSLIB.Ros({
        url : 'ws://localhost:9090'
    });

    ros.on('connection', function() {
        clearInterval(rosConnector);
        console.log("Connected to ROS");
        CheckFullRosStartup();
    });
}

// Once a connection to ROS is made then this function is called which waits for
// a signal from the nao_top_camera. It then lets the user start the experiment.
function CheckFullRosStartup(){
    var listener = new ROSLIB.Topic({
        ros : ros,
        name : '/nao_top_camera',
        messageType : 'sensor_msgs/Image'
    });

    listener.subscribe(function(message) {     
        textStartCode.style.display = "inline";
        btnStartCode.style.display = "inline";
        document.getElementById("restartTimer").innerHTML = "<input id='btnPowerOn' type='button' value='PowerOn' class='btn-style-green' onclick='PowerOn()' hidden>";
        listener.unsubscribe();
    });
}

// Using the usernode object created by the user, this function injects the required Python code and 
// then sends the code to the userplancs_server which will launch it as a node in ROS.
function createNode(){
    var creator = new ROSLIB.Topic({
      ros : ros,
      name : '/node_created',
      messageType : 'std_msgs/String'
    });

    var name = userSenseNodes[0].Name + "\")";
    var publisher = userSenseNodes[0].Name + "\", Bool, queue_size=10)";
    userSenseNodes[0].MessageType = "std_msgs/Bool";
    userSenseNodes[0].Publishers = "/" + userSenseNodes[userSenseNodes.length - 1].Name;
    var subscribedTopics = [];
    var callbacks = [];
    var thresholds = [];
    var thresholdSettings = [];
    var numberOfCallbacks = 0;
    var timestamps = [];
    var globals = [];
    var booleans = [];

    var rsAngle = $("#rangeSliderAngle").data("ionRangeSlider");
    var rsDistance = $("#rangeSliderDistance").data("ionRangeSlider");

    // For every new topic that is added as a choice when creating a new sense node, the following needs to be added.
    if (userSenseNodes[0].Subscribers.indexOf("/plancs_angle_distance") !== -1){
        subscribedTopics.push("mynode.add_subscriber(\"plancs_angle_distance\", BoxSense, box_sense_callback)");
                           
        callbacks.push(["def box_sense_callback(data):",
                        "    global angDistThresh, boxSenseTimeStamp",
                        "    angDistThresh = False",
                        "    boxSenseTimeStamp = time.time()",
                        "    upperThresholdAng = " + rsAngle.old_to ,
                        "    lowerThresholdAng = " + rsAngle.old_from,
                        "    upperThresholdDist = " + rsDistance.old_to,
                        "    lowerThresholdDist = " + rsDistance.old_from,
                        "    distance = data.distance",
                        "    angle = data.degrees",
                        "    if angle < upperThresholdAng and angle > lowerThresholdAng:",
                        "        if distance < upperThresholdDist and distance > lowerThresholdDist:",
                        "            angDistThresh = True"]);

        globals.push("angDistThresh");
        globals.push("boxSenseTimeStamp");

        booleans.push("angDistThresh");

        thresholdSettings.push("    angDistThresh = False");
        thresholdSettings.push("    boxSenseTimeStamp = time.time() + 60");

        timestamps.push("        if boxSenseTimeStamp < time.time() - 1.0:");
        timestamps.push("            angDistThresh = False");

        numberOfCallbacks++;
    }
    // -------------------------------------------------------------------------------------------------------------


    if (userSenseNodes[0].Subscribers.indexOf("/plancs_green_stripe") !== -1){
        subscribedTopics.push("mynode.add_subscriber(\"plancs_green_stripe\", Bool, green_stripe_sense_callback)");
               
        callbacks.push(["def green_stripe_sense_callback(data):",
                        "    global greenStrip, greenStripTimeStamp",
                        "    greenStripTimeStamp = time.time()",
                        "    greenStrip = True"]);

        globals.push("greenStrip");
        globals.push("greenStripTimeStamp");

        booleans.push("greenStrip");

        thresholdSettings.push("    greenStrip = False");
        thresholdSettings.push("    greenStripTimeStamp = time.time() + 60");

        timestamps.push("        if greenStripTimeStamp < time.time() - 2.0:");
        timestamps.push("            greenStrip = False");

        numberOfCallbacks++;
    }
    code = "";

    // generic_node.js
    // ---------------------------------------
    for (var i = 0; i < generic_node.length; i++)       // Checking line by line and adding code.
    {
        if (i === 6){
            code += generic_node[i];
            for (var j = 0; j < globals.length; j++){
                code += globals[j];
                if (j != globals.length - 1)
                    code += ", ";
            }
            code += "\n";
        }
        else if (i === 7){
            for (var j = 0; j < thresholdSettings.length; j++){
                code += generic_node[i] + thresholdSettings[j] + "\n";
            }
        }
        else if (i === 9)
            code += generic_node[i] + numberOfCallbacks + "\n";
        else if (i === 10)
            code += generic_node[i] + name +"\n";
        else if (i === 11)
        {
            for (var l = 0; l < subscribedTopics.length; l++){
                code += generic_node[i] + subscribedTopics[l] +"\n";
            }
        }
        else if (i === 12)
            code += generic_node[i] + publisher +"\n";
        else if (i === 16){
            for (var j = 0; j < booleans.length; j++){
                code += generic_node[i] + booleans[j] + ")\n";
            }
        }
        else if (i === 17){
            for (var j = 0; j < timestamps.length; j++){
                code += generic_node[i] + timestamps[j] + "\n";
            }
        }
        else if (i === 29)
        {
            for (var j = 0; j < callbacks.length; j++){
                for (var k = 0; k < callbacks[j].length; k++){
                    code += generic_node[i] + callbacks[j][k] +"\n";
                }
            }
        }
        else
            code += generic_node[i] + "\n";
    }
    // ---------------------------------------

    var create_message = new ROSLIB.Message({
      data : code
    });

    //console.log(code);
      
    creator.publish(create_message);
}

// Gets system node names from ROS.
function getSystemNodes(){
    var getSysNodeNames = new ROSLIB.Service({
        ros : ros,
        name : '/system_get_nodes',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({
        request : "rosnode list"
    });

    getSysNodeNames.callService(requester, function(result){
        var splitString = result.response.split("\n");
        for (var i = 0; i < splitString.length; i++){
        	var newNode = new systemSenseNode();
			newNode.Name = splitString[i];
			//newNode.TimeStamp.push(new Date().getTime());
			systemSenseNodes[i] = newNode;
        }
		for (var i = 0; i < systemSenseNodes.length; i++){
    		getTopics(systemSenseNodes[i]);
    	}
    });
}

// When a message is received on this topic then that means
// the experiment has ended in Webots so an overlay is shown
// to the user saying the experiment has ended.
function CheckKillRosbagRecording(){
    var rosbagKilled = new ROSLIB.Topic({
        ros : ros,
        name : 'kill_rosbag',
        messageType : 'std_msgs/String'
    });

    rosbagKilled.subscribe(function(message) {
        document.getElementById("endExperimentOverlay").style.display = "inline";
    });
}

// When the 'Power On' button is pressed this function is called which
// starts recording data from the experiment into ROSbags.
function StartBagRecording(){
    var BagRecording = new ROSLIB.Service({
        ros : ros,
        name : '/rosbag_recorder',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({
        request : startCode
    });

    BagRecording.callService(requester, function(result){
    });
}   

// When the 'Start New Experiment' button is pressed (to stop the current experiment) 
//then if it has not already been stopped from within Webots, this function
// will stop recording data in ROSbags.
function StopBagRecording(){
    var BagRecording = new ROSLIB.Service({
        ros : ros,
        name : '/rosbag_recorder',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({
        request : "stop"
    });

    BagRecording.callService(requester, function(result){
    });
}



// Gets user node names from ROS once the user has launched their node.
function getUserNodes(){
    document.getElementById("divUserSenseNodes").innerHTML = "";
    document.getElementById("divUserSenseStatus").innerHTML = "";
    document.getElementById("parUserSenseData").innerHTML = "";

    var getUserNodeNames = new ROSLIB.Service({
        ros : ros,
        name : '/user_get_nodes',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({
        request : "rosnode list"
    });

    getUserNodeNames.callService(requester, function(result){
        var splitString = result.response.split("\n");
    });

    setTimeout("TrackUserSenseConnections()", 2000);
}

// Gets topic and message type from the ROS node handed in.
function getTopics(node){
    topics = [];
    msgtypes = [];
    var getTopic = new ROSLIB.Service({
      ros : ros,
      name : '/get_topics',
      messageType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({
        request : "rosnode info " + node.Name
    });

    getTopic.callService(requester, function(result){
        getTopicsHelper(result, node);
    });
}  

// A helper function for getting topics and message types from a ROS Node
function getTopicsHelper(result, node){
	var data;
	var topics = [];
	var msgtypes = [];
        data = result.response.split(",");
        for (var i = 0; i < data.length; i++)
        {   if (data[i] !== "/rosout" || data[i] !== "rosgraph_msgs/Log"){
                if (i % 2 == 0)
                    topics.push(data[i]);
                else
                    msgtypes.push(data[i]);
            }
        }
        node.Topics = topics;
        node.MessageTypes = msgtypes;

}

// When a ROS node is deleted by the user this function will then stop and 
// delete the ROS node from running in ROS
function KillNodeService(node){
    var killNode = new ROSLIB.Service({
        ros : ros,
        name : '/userplancs_killer',
        serviceType : 'userplancs/UserplancsString'
    });

    var request = new ROSLIB.ServiceRequest({
        request : node
    });

    killNode.callService(request, function(result){ });

}
// Uses MJPEG Server to display video feeds.
function VideoFeed(view){
	if (view == 1){
		var viewer = new MJPEGCANVAS.Viewer({		//MJPEGCANVAS.MultiStreamViewer
			divID : 'mjpeg',
			host : 'localhost',
			port : 8181,
			width : 320,
			height : 240,
			topic : 'nao_top_camera',
			quality : 100
		});
	}	
}


// Enables the user to enable or disable stiffness to the Nao. The parameter specifies
// whether to enable or disable stiffness. 
function ChangeStiffness(enable){
	var bodyStiffness = new ROSLIB.Service({
		ros : ros,
		name : '/nao_ALMotion_setStiffnesses',
		serviceType : 'plancs/SALMotion_setStiffnesses'
	});

	var request = new ROSLIB.ServiceRequest({
		names : 'Body',
		stiffnesses : enable
	});

	bodyStiffness.callService(request, function(result){
	});
}

// When training mode is set to be on then certain things the robot
// does should not be allowed to happen. 
function InhibitNodes(){
    var userNodeInhibiter = new ROSLIB.Topic({
      ros : ros,
      name : '/userplancs_newsense/plancs_inhibit',
      messageType : 'std_msgs/Empty',
      queue_size : 10
    });

    var joyNodeInhibiter = new ROSLIB.Topic({
      ros : ros,
      name : '/joy_to_nao_node/plancs_inhibit',
      messageType : 'std_msgs/Empty',
      queue_size : 10
    });

    var empty_message = new ROSLIB.Message({});
      
    userNodeInhibiter.publish(empty_message);
    joyNodeInhibiter.publish(empty_message);
}  

// When the experiment is started or ended a message is published
// which is recorded in ROSbag.
function StartStopMessages(){
    var startStop = new ROSLIB.Topic({
      ros : ros,
      name : '/start_end_experiment',
      messageType : 'std_msgs/Empty'
    });
    var empty_message = new ROSLIB.Message({});  
    startStop.publish(empty_message);
}

// This function accepts a message (a name of an image file) it receives from the supervisor
// node in Webots and displays the image. Everytime a new message is received it displays that
// image. This simulates a video stream.
function DisplayWebotsImages(){
    var listener = new ROSLIB.Topic({
        ros : ros,
        name : '/webots_image',
        messageType : 'std_msgs/String'
    });

    listener.subscribe(function(message) {
        imgSrc = message.data;
        img = document.getElementById("webotsImage");
        img.src = "../WUI_userplancs/webotsImages/" + imgSrc;
    });
}