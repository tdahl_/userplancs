 #!/usr/bin/env python
import sys, rospy, os, plancs, time
from std_msgs.msg import *
from plancs import *
from userplancs.msg import *

def main():
    global angDistThresh, boxSenseTimeStamp, greenStrip, greenStripTimeStamp
    angDistThresh = False
    boxSenseTimeStamp = time.time() + 60
    greenStrip = False
    greenStripTimeStamp = time.time() + 60
    okPublish = False
    numberOfCallbacks = 2
    mynode = plancs.Plancs_Node("userplancs_newsense")
    sub = mynode.add_subscriber("plancs_angle_distance", BoxSense, box_sense_callback)
    sub = mynode.add_subscriber("plancs_green_stripe", Bool, green_stripe_sense_callback)
    publisher = rospy.Publisher("userplancs_newsense", Bool, queue_size=10)
    tempArray = []
    mynode.set_decay_period(2.0)
    while not rospy.is_shutdown():
        tempArray.append(angDistThresh)
        tempArray.append(greenStrip)
        if boxSenseTimeStamp < time.time() - 1.0:
            angDistThresh = False
        if greenStripTimeStamp < time.time() - 2.0:
            greenStrip = False
        if len(tempArray) == numberOfCallbacks:
            for i in range(len(tempArray)):
                if tempArray[i] == False:
                    okPublish = False
                    break
            else:
                    okPublish = True
            if okPublish == True:
                publisher.publish(True)
        tempArray = []
        rospy.sleep(0.1)

def box_sense_callback(data):
    global angDistThresh, boxSenseTimeStamp
    angDistThresh = False
    boxSenseTimeStamp = time.time()
    upperThresholdAng = 3.0
    lowerThresholdAng = -3.0
    upperThresholdDist = 0.14
    lowerThresholdDist = 0
    distance = data.distance
    angle = data.degrees
    if angle < upperThresholdAng and angle > lowerThresholdAng:
        if distance < upperThresholdDist and distance > lowerThresholdDist:
            angDistThresh = True

def green_stripe_sense_callback(data):
    global greenStrip, greenStripTimeStamp
    greenStripTimeStamp = time.time()
    greenStrip = True

if __name__ == '__main__':
    main()