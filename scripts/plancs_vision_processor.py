#!/usr/bin/env python

import cv2, sys, rospy, os, plancs, almath
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import JointState
from std_msgs.msg import Bool
from plancs import *
from userplancs.msg import BoxSense

import roslib; roslib.load_manifest('userplancs')

bridge = CvBridge()
imageData = None

# Subscribes to nao_top_camera and searches the image for colours. which are different objects.
# If the red beam is found then it calculates the distance and angle to the beam.
def get_image():
    global imageData, done, count, joints
    count = 1
    done = False

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    argv = rospy.myargv(argv=sys.argv)

    mynode = plancs.Plancs_Node(name)
    sub = mynode.add_subscriber("nao_top_camera", Image, callback)
    mynode.add_subscriber("joint_states", JointState, jointStates)

    boxSense = BoxSense()
    boxSensePub = rospy.Publisher("plancs_angle_distance", BoxSense, queue_size=10)
    greenStripPub = rospy.Publisher("plancs_green_stripe", Bool, queue_size=10)
    blueStripPub = rospy.Publisher("plancs_blue_stripe", Bool, queue_size=10)
    anglePerPixel = None
    joints = None

    while not rospy.is_shutdown(): 
	if imageData != None:
	    if done == False:
		blueFound = False
		greenFound = False
	        boxMinX = -1
    	        boxMinY = -1
	        boxMaxX = -1
	        boxMaxY = -1

		img = bridge.imgmsg_to_cv2(imageData, "bgr8")
	    	rows, cols, depth = img.shape
		centerImage = cols / 2

		np_img = np.asarray(img)
		
		########################################################################################
		# Used during testing. Detect either R, G or B and colour it another colour.
		# Use cv2.imshow to view the image with colour altered.

		#Detect green:
		#np_img[np.where(((np_img >= [0,140,0]) & (np_img <= [10,180,10])).all(axis = 2))] = [252,255,42]

		#Detect blue:
		#np_img[np.where(((np_img >= [220,0,0]) & (np_img <= [255,10,10])).all(axis = 2))] = [247,42,255]

		#Detect red:
		#np_img[np.where((((np_img >= [0,0,200]) & (np_img <= [10,10,255])) | ((np_img >= [0,0,120]) & (np_img <= [10,10,140]))).all(axis = 2))] = [42,255,82]

		#cv2.imshow("Image window", np_img)
		#cv2.waitKey(3)
		########################################################################################

		# Search for blue stripe:
		indices = np.where(((np_img >= [220,0,0]) & (np_img <= [255,10,10])).all(axis = 2))
		zipp = zip(indices[0], indices[1])
		if len(zipp) > 0:
		    blueFound = True

		# Search for green stripe
		indices = np.where(((np_img >= [0,140,0]) & (np_img <= [10,180,10])).all(axis = 2))
		zipp = zip(indices[0], indices[1])
		if len(zipp) > 0:
		    greenFound = True

		# Search for red beam
		indices = np.where((((np_img >= [0,0,200]) & (np_img <= [10,10,255])) | ((np_img >= [0,0,120]) & (np_img <= [10,10,140]))).all(axis = 2))
		zipp = zip(indices[0], indices[1])
		boxHeight = 0
	        boxWidth = 0

		########################################################################################
		# This following loop is required because 'zipp' is an array of the 
		# first found (x,y) coordinates of the red beam to the last found (x,y) coordinates. 
		# When the robot is at an angle to the beam then the top or bottom of the beam becomes 
		# zigzag shaped and so the first found red (x,y) coordinates would not be top left 
		# but top right. The same applies for bottom right. It would be found to be bottom left. 
		# This loop finds the max red (x,y) and min red (x,y). Without doing this, the distance
		# calculation is almost always incorrect.
		########################################################################################

		if len(zipp) > 0:
	            boxMinX = zipp[0][1]
    	            boxMinY = zipp[0][0]
	            boxMaxX = zipp[len(zipp)-1][1]
	            boxMaxY = zipp[len(zipp)-1][0]
		    for x in range(len(zipp)-1, -1, -1):
			if zipp[x][1] > boxMaxX:
			    boxMaxX = zipp[x][1]
			if zipp[x][0] > boxMaxY:
			    boxMaxY = zipp[x][0]
			if zipp[x][1] < boxMinX:
			    boxMinX = zipp[x][1]
			if zipp[x][0] < boxMinY:
			    boxMinY = zipp[x][0]

		if anglePerPixel == None:
		    # 61 Degrees =  1.06465084 rads : Viewing angle of Nao camera 
		    anglePerPixel = 1.06465084 / cols  # Angle per pixel to turn.
		
		if boxMinX != -1 and boxMaxX != -1 and boxMinY != -1 and boxMaxY != -1:
		    boxWidth = boxMaxX - boxMinX
   		    boxHeight = boxMaxY - boxMinY
		    boxCenterX = boxMinX + boxWidth / 2

		    # Angle body is turned away from object
		    pixelsFromCenter = centerImage - boxCenterX		    
		    angle = pixelsFromCenter * anglePerPixel

		    if joints != None:
            		headYaw = joints.position[0]
			if headYaw > 0.001 or headYaw < -0.001:
			    if angle > 0.0 and headYaw > 0.0:
				angle += headYaw
			    elif angle > 0.0 and headYaw < 0.0:
				angle += headYaw
			    elif angle < 0.0 and headYaw < 0.0:
				angle += headYaw
			    else:
				angle += headYaw

		    ########################################################################
		    # Calculating distance from robot to object uses:
		    # dist = object_width * focal_length_of_camera / object_width_in_pixels
		    #
		    # where:
		    # focal_length_of_camera is f = d * Z / D
		    # f = focalLength
		    # Z = Distance to object
		    # d = width in pixels of object from distance Z
		    # D = actual height of object
		    ########################################################################
		    focalLength = 290
		    if boxWidth > 0:
			distance = 0.15 * focalLength / boxWidth
			boxSense.distance = distance 
			boxSense.degrees = angle * almath.TO_DEG
			boxSensePub.publish(boxSense)

		if greenFound == True:
		    greenStripPub.publish(Bool(1))
		if blueFound == True:
			blueStripPub.publish(Bool(1))			
		done = True
		rospy.sleep(0.4)

# Everytime a new image is received from nao_top_camera.
def callback(data):
    global imageData, done, count
    if count == 1:
	imageData = data
        count += 1
    if done == True:
	imageData = data
	done = False

# Updates the joint values of the Nao.
def jointStates(jointValues):
    global joints
    joints = jointValues

if __name__ == '__main__':
    get_image()
