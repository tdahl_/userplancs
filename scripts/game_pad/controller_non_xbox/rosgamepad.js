
// Connecting to ROS
// -----------------
var ros = new ROSLIB.Ros({
  url : 'ws://localhost:9090'
});
//------------------

var haveEvents = 'GamepadEvent' in window;
var controllers = {};

var buttonDataOld = [];
var axesDataOld = [];
var moveBlocker = 0;

var rAF = window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.requestAnimationFrame;

function connecthandler(e) {
  addgamepad(e.gamepad);
}

function addgamepad(gamepad) {
  controllers[gamepad.index] = gamepad; 
  console.log("Gamepad Connected");
  rAF(updateStatus);
}

function disconnecthandler(e) {
  removegamepad(e.gamepad);
}

function removegamepad(gamepad) {
  delete controllers[gamepad.index];
}

// Send gamepad data to ROS via rosbridge server
//----------------------------------------------
function sendData(buttonData, axesData){
  var gamePadData = new ROSLIB.Topic({
    ros : ros,
    name : '/controller',
    messageType : 'userplancs/GamePad'
  });

  var message = new ROSLIB.Message({
    buttons: buttonData,
    axes   : axesData 
  });
  gamePadData.publish(message);
}
//----------------------------------------------



function updateStatus() {

  moveBlocker++;

  if (moveBlocker % 100 == 0)
    checkMoveIsActive();

  if (moveBlocker == 1000)
    moveBlocker = 0;

  if (!haveEvents) {
    scangamepads();
  }
  for (j in controllers) {
    var controller = controllers[j];

    var buttonData = [];
    var axesData = [];

    for (var i=0; i<controller.buttons.length; i++) {
      var val = controller.buttons[i];
      var pressed = val == 1.0;
      if (typeof(val) == "object") {
        pressed = val.pressed;
        val = val.value;
      }
      var pct = Math.round(val * 100) + "%"

      if (pressed) {
        buttonData[i] = 1.0;
      } else {
        buttonData[i] = 0.0;
      }
    }

    for (var i=0; i<controller.axes.length; i++) {
      //axesData[i] = controller.axes[i] * -1;
      axesData[i] = controller.axes[i];
    }
  }

  

  // Detect button or axes data changed
  //-----------------------------------
  if (buttonDataOld.length === 0 || axesDataOld.length === 0) 
  {
    
    sendData(buttonData, axesData);
    buttonDataOld = buttonData;
    axesDataOld = axesData;
  }
  else 
  {
    var changedButton = false;
    var changedAxes = false;
    for (var i = 0; i < buttonData.length; i++)
    {
      if (buttonData[i] != buttonDataOld[i])
      {
        changedButton = true;
        break;
      }
    }


    for (var i = 0; i < axesData.length; i++)
    {
        if (axesData[i] > 0)
        {
            if (axesData[i] > axesDataOld[i] + 0.5 || axesData[i] < axesDataOld[i] - 0.5)
            {   
                if (axesData[i] >= 0.8 || axesData[i] <= 0.2)
                {
                    changedAxes = true;
                    break;
                }
            }
        }


        else if (axesData[i] < 0)
        {
            if (axesData[i] < axesDataOld[i] - 0.5 || axesData[i] > axesDataOld[i] + 0.5) 
            {
                if (axesData[i] <= -0.8 || axesData[i] >= -0.2 )
                {
                    changedAxes = true;
                    break;
                }
            }
        }
    }


    if (changedButton || changedAxes) 
    {
      sendData(buttonData, axesData);
      buttonDataOld = buttonData;
      axesDataOld = axesData;
      if (buttonData[9] === 1){
        checkCoupling();
      }
    }

}
  //-----------------------------------
  rAF(updateStatus);
}

function checkCoupling(){

    var coupleArms = new ROSLIB.Topic({
        ros : ros,
        name : '/check_coupling',
        messageType : 'std_msgs/Bool'
    });

    coupleArms.subscribe(function(message) {     
        if (message.data)
          TurnLEDON("imgCouplingStatus");  
        else
          TurnLEDOFF("imgCouplingStatus");

        coupleArms.unsubscribe();
    });
}

function checkMoveIsActive(){
   var checkMove = new ROSLIB.Service({
        ros : ros,
        name : '/nao_ALMotion_moveIsActive',
        serviceType : 'SALMotion_moveIsActive'
    });

    var requester = new ROSLIB.ServiceRequest({
    });

    checkMove.callService(requester, function(result){
      if (result.reply)
        TurnLEDON("imgWalkingStatus");
      else
        TurnLEDOFF("imgWalkingStatus");
    });
}

function scangamepads() {
  var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
  for (var i = 0; i < gamepads.length; i++) {
    if (gamepads[i]) {
      if (!(gamepads[i].index in controllers)) {
        addgamepad(gamepads[i]);
      } else {
        controllers[gamepads[i].index] = gamepads[i];
      }
    }
  }
}

window.addEventListener("gamepadconnected", connecthandler);
window.addEventListener("gamepaddisconnected", disconnecthandler);
if (!haveEvents) {
  setInterval(scangamepads, 500);
}