#!/usr/bin/env python
##################################################
#
#   Description: joy_to_nao_node aims to control a 
#    Nao Robot with a Joystick controller. It
#    catches the message from the topic "/joy" and
#    then interact with Nao.
#
#   Commands:
#       - Arms: Analog sticks and 
#           4 top buttons (+z, -z axis)
#       - Head: 4 buttons on the right side
#       - Walking: directional pad (x, y axis) 
#          + top-left/right to rotate 
#   
#   Note: 
#    Don't forget to change the IP variable
#    IP = "127.0.0.1"   #Simu Robot 
#       
#
#   Mail Contact: clenet.baptiste@gmail.com
##################################################


import rospy, plancs, os, sys
from std_msgs.msg import *
from sensor_msgs.msg import Joy
from plancs import *
from userplancs.msg import GamePad
from userplancs.srv import *


'''
Initialize proxy with Nao Robot
Subsribe to topic "/joy"
Send command to Nao
'''
class joy_to_nao_class():
    firstCallback = False
    coupleArms = True
    checkCoupling = None

    def __init__(self, IP, PORT):
        
        self.motionProxy = PALProxy("ALMotion")
        self.postureProxy = PALProxy("ALRobotPosture")
            
        rospy.sleep(2.0)

        #Enable Arm Control during the walk
        self.motionProxy.setWalkArmsEnabled(False,False)

        self.checkCoupling = rospy.Publisher("/check_coupling", Bool, queue_size=0)
        
        self.listener()
        

    def callback(self,data):

        

        if self.firstCallback == True:
            self.firstCallback = False
        else:
            x  = 0.0
            y  = 0.0
            theta  = 0.0
            frequency  = 0.5
            if len(data.buttons) != 0 and len(data.axes) != 0:
                if data.buttons[8] == 1:
                    self.postureProxy.post.goToPosture("StandZero", 1.0)
                    self.firstCallback = True
                else:
                    #Walking             
                    if data.buttons[0] == 1:
                        x = -1
                    elif data.buttons[3] == 1:
                        x = 1
                    else:
                        x = 0;

                    if data.buttons[2] == 1:
                        y = 1
                    elif data.buttons[1] == 1:
                        y = -1
                    else:
                        y = 0

                    if x == 1 and y == 1:
                        theta =  1 
                        x = y = 0
                    elif x == 1 and y == -1: 
                        theta = -1
                        x = y = 0
                    else :
                        theta = 0

                    if data.buttons[1] == 1 and data.buttons[3] == 1:
                        theta =  -1 
                        x = y = 0
                    elif data.buttons[0] == 1 and data.buttons[2] == 1:
                        theta =  1 
                        x = y = 0

                    self.motionProxy.setWalkTargetVelocity(x, y, theta, frequency)
                    
                    if self.coupleArms == True:
                        # Both arms at same time
                        if data.axes[1] < -0.8 or data.axes[3] < -0.8:
                            self.motionProxy.post.changeAngles("LShoulderPitch",-2,0.1)
                            self.motionProxy.post.changeAngles("RShoulderPitch",-2,0.1)
                        elif data.axes[1] > 0.8 or data.axes[3] > 0.8:
                            self.motionProxy.post.changeAngles("LShoulderPitch",2,0.1)
                            self.motionProxy.post.changeAngles("RShoulderPitch",2,0.1)
                        else:
                            self.motionProxy.post.changeAngles("LShoulderPitch",0,0.1)
                            self.motionProxy.post.changeAngles("RShoulderPitch",0,0.1)

                        if data.axes[0] > 0.8 or data.axes[2] > 0.8:
                            self.motionProxy.post.changeAngles("LElbowRoll",2,0.1)
                            self.motionProxy.post.changeAngles("RElbowRoll",-2,0.1)
                        elif data.axes[0] < -0.8 or data.axes[2] < -0.8:
                            self.motionProxy.post.changeAngles("LElbowRoll",-2,0.1)
                            self.motionProxy.post.changeAngles("RElbowRoll",2,0.1)
                        else:
                            self.motionProxy.post.changeAngles("LElbowRoll",0,0.1)
                            self.motionProxy.post.changeAngles("RElbowRoll",0,0.1)

                        if data.buttons[4] == 1 or data.buttons[5] == 1:
                            self.motionProxy.post.changeAngles("LShoulderRoll",-2,0.1)
                            self.motionProxy.post.changeAngles("RShoulderRoll",2,0.1)
                        elif data.buttons[6] == 1 or data.buttons[7] == 1:
                            self.motionProxy.post.changeAngles("LShoulderRoll",2,0.1)
                            self.motionProxy.post.changeAngles("RShoulderRoll",-2,0.1)
                        else :
                            self.motionProxy.post.changeAngles("LShoulderRoll",0,0.1)
                            self.motionProxy.post.changeAngles("RShoulderRoll",0,0.1)
                    else:
                        #Left Arm Motion
                        if data.axes[1] < -0.8:
                            self.motionProxy.post.changeAngles("LShoulderPitch",-2,0.1)
                        elif data.axes[1] > 0.8:
                            self.motionProxy.post.changeAngles("LShoulderPitch",2,0.1)
                        else:
                            self.motionProxy.post.changeAngles("LShoulderPitch",0,0.1)

                        if data.axes[0] > 0.8:
                            self.motionProxy.post.changeAngles("LElbowRoll",-2,0.1)
                        elif data.axes[0] < -0.8:
                            self.motionProxy.post.changeAngles("LElbowRoll",2,0.1)
                        else:
                            self.motionProxy.post.changeAngles("LElbowRoll",0,0.1)
                        if data.buttons[4] == 1:
                            self.motionProxy.post.changeAngles("LShoulderRoll",-2,0.1)
                        elif data.buttons[6] == 1:
                            self.motionProxy.post.changeAngles("LShoulderRoll",2,0.1)
                        else :
                            self.motionProxy.post.changeAngles("LShoulderRoll",0,0.1)

                        #Right Arm Motion
                        if data.axes[3] < -0.8:
                            self.motionProxy.post.changeAngles("RShoulderPitch",-2,0.1)
                        elif data.axes[3] > 0.8:
                            self.motionProxy.post.changeAngles("RShoulderPitch",2,0.1)
                        else:
                            self.motionProxy.post.changeAngles("RShoulderPitch",0,0.1)
                        if data.axes[2] > 0.8:
                            self.motionProxy.post.changeAngles("RElbowRoll",-2,0.1)
                        elif data.axes[2] < -0.8: #< -0.8:
                            self.motionProxy.post.changeAngles("RElbowRoll",2,0.1)
                        else:
                            self.motionProxy.post.changeAngles("RElbowRoll",0,0.1)
                        if data.buttons[5] == 1:
                            self.motionProxy.post.changeAngles("RShoulderRoll",2,0.1)
                        elif data.buttons[7] == 1:
                            self.motionProxy.post.changeAngles("RShoulderRoll",-2,0.1)
                        else :
                            self.motionProxy.post.changeAngles("RShoulderRoll",0,0.1)
                    
                    #Head Motion
                    if data.buttons[14] == 1:
                        self.motionProxy.post.changeAngles("HeadYaw",2,0.1)
                    elif data.buttons[15] == 1:
                        self.motionProxy.post.changeAngles("HeadYaw",-2,0.1)
                    else :
                        self.motionProxy.post.changeAngles("HeadYaw",0,0.1)
                    if data.buttons[13] == 1:
                        self.motionProxy.post.changeAngles("HeadPitch",2,0.1)
                    elif data.buttons[12] == 1:
                        self.motionProxy.post.changeAngles("HeadPitch",-2,0.1)
                    else :
                        self.motionProxy.post.changeAngles("HeadPitch",0,0.1)
                            
                    if data.buttons[9] == 1:
                        if self.coupleArms == True:
                            self.coupleArms = False
                        else:
                            self.coupleArms = True
                        self.checkCoupling.publish(self.coupleArms)

    def listener(self):
        global last_callback_time

        name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
        argv = rospy.myargv(argv=sys.argv)

        mynode = plancs.Plancs_Node(name)
        sub = mynode.add_subscriber("controller", GamePad, self.callback)
        self.motionProxy.setStiffnesses("Body", 1.0)
        mynode.set_decay_period(2.0)
        while not rospy.is_shutdown():
            if mynode.inhibited:
                self.motionProxy.killMove()
            rospy.sleep(0.2)


if __name__ == '__main__':
    IP = "127.0.0.1"   #Simu Robot
    PORT = 9559
    
    joy_to_nao_class(IP,PORT)