
// Connecting to ROS
// -----------------
var ros = new ROSLIB.Ros({
  url : 'ws://localhost:9090'
});
//------------------

var buttonDataOld = [];
var axesDataOld = [];
var moveBlocker = 0;

var haveEvents = 'ongamepadconnected' in window;
var controllers = {};
var rAF = window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame;

function connecthandler(e) {
  addgamepad(e.gamepad);
}

function addgamepad(gamepad) {
  controllers[gamepad.index] = gamepad;

  rAF(updateStatus);
}

function disconnecthandler(e) {
  removegamepad(e.gamepad);
}

function removegamepad(gamepad) {
  delete controllers[gamepad.index];
}

// Send gamepad data to ROS via rosbridge server
//----------------------------------------------
function sendData(buttonData, axesData){
  var gamePadData = new ROSLIB.Topic({
    ros : ros,
    name : '/controller',
    messageType : 'userplancs/GamePad'
  });

  var message = new ROSLIB.Message({
    buttons: buttonData,
    axes   : axesData 
  });
  gamePadData.publish(message);
}
//----------------------------------------------

function updateStatus() {
  moveBlocker++;

  if (moveBlocker % 100 == 0)
    checkMoveIsActive();
  if (moveBlocker == 1000)
    moveBlocker = 0;

  if (!haveEvents) {
    scangamepads();
  }

  var i = 0;
  var j;

  for (j in controllers) {
    var controller = controllers[j];

    var buttonData = [];
    var axesData = [];

    for (i = 0; i < controller.buttons.length; i++) {
      var val = controller.buttons[i];
      var pressed = val == 1.0;
      if (typeof(val) == "object") {
        pressed = val.pressed;
        val = val.value;
      }

      if (pressed) {
        buttonData[i] = 1.0;
      } else {
        buttonData[i] = 0.0;
      }
    }
  }

  for (var i=0; i<controller.axes.length; i++) {
      //axesData[i] = controller.axes[i] * -1;
      axesData[i] = controller.axes[i];
  }

    // Detect button or axes data changed
  //-----------------------------------
  if (buttonDataOld.length === 0 || axesDataOld.length === 0) 
  {
    sendData(buttonData, axesData);
    buttonDataOld = buttonData;
    axesDataOld = axesData;
  }
  else 
  {
    var changedButton = false;
    var changedAxes = false;
    for (var i = 0; i < buttonData.length; i++)
    {
      if (buttonData[i] != buttonDataOld[i])
      {
        changedButton = true;
        break;
      }
    }


    for (var i = 0; i < axesData.length; i++)
    {
        if (axesData[i] > 0)
        {
            if (axesData[i] > axesDataOld[i] + 0.5 || axesData[i] < axesDataOld[i] - 0.5)
            {   
                if (axesData[i] >= 0.8 || axesData[i] <= 0.2)
                {
                    changedAxes = true;
                    break;
                }
            }
        }


        else if (axesData[i] < 0)
        {
            if (axesData[i] < axesDataOld[i] - 0.5 || axesData[i] > axesDataOld[i] + 0.5) 
            {
                if (axesData[i] <= -0.8 || axesData[i] >= -0.2 )
                {
                    changedAxes = true;
                    break;
                }
            }
        }
    }

    if (changedButton || changedAxes) 
    {
      sendData(buttonData, axesData);
      buttonDataOld = buttonData;
      axesDataOld = axesData;

      if (buttonData[9] === 1){
        checkCoupling();
      }

      if (buttonData[10] === 1){
        if (typeof leftStickBlock === 'undefined'){
          liftBox();  
        }
        if (typeof humanRecord !== 'undefined'){
          doUserMotion();
        }
          
      }


      if (buttonData[11] === 1){
        dropBox();
      }
    }
  }
  //-----------------------------------


  rAF(updateStatus);
}

function liftBox() {
  var grabMotion = new ROSLIB.Service({
        ros : ros,
        name : '/grab_motion',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({});

    grabMotion.callService(requester, function(result){});
}

function dropBox() {
  var grabMotion = new ROSLIB.Service({
        ros : ros,
        name : '/drop_motion',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({});

    grabMotion.callService(requester, function(result){});
}

function doUserMotion(){
      if (typeof motionLength !== "undefined"){
        if (motionLength < 10)
          setTimeout("AllowNextMotion()", (motionLength + (motionLength * 0.4)) * 1000);  
        else if (motionLength < 30)
          setTimeout("AllowNextMotion()", (motionLength + (motionLength * 0.2)) * 1000);  
        else
          setTimeout("AllowNextMotion()", (motionLength + (motionLength * 0.1)) * 1000);  

        setTimeout("TurnBoxRed('parSystemMotionStatus0')", (motionLength + (motionLength * 0.1))  * 1000);
      }
      else{
        setTimeout("AllowNextMotion()", motionLength + 1000);
        setTimeout("TurnBoxRed('parSystemMotionStatus0')", motionLength);
      }
      TurnBoxGreen("parSystemMotionStatus0");
    var grabMotion = new ROSLIB.Service({
        ros : ros,
        name : '/user_motion',
        serviceType : 'userplancs/UserplancsString'
    });

    var requester = new ROSLIB.ServiceRequest({});

    grabMotion.callService(requester, function(result){});
}

function checkCoupling(){

    var coupleArms = new ROSLIB.Topic({
        ros : ros,
        name : '/check_coupling',
        messageType : 'std_msgs/Bool'
    });

    coupleArms.subscribe(function(message) {     
        if (message.data)
          TurnLEDON("imgCouplingStatus");  
        else
          TurnLEDOFF("imgCouplingStatus");

        coupleArms.unsubscribe();
    });
}

function checkMoveIsActive(){
   var checkMove = new ROSLIB.Service({
        ros : ros,
        name : '/nao_ALMotion_moveIsActive',
        serviceType : 'SALMotion_moveIsActive'
    });

    var requester = new ROSLIB.ServiceRequest({
    });

    checkMove.callService(requester, function(result){
      if (result.reply)
        TurnLEDON("imgWalkingStatus");
      else
        TurnLEDOFF("imgWalkingStatus");
    });
  
}

function scangamepads() {
  var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
  for (var i = 0; i < gamepads.length; i++) {
    if (gamepads[i]) {
      if (gamepads[i].index in controllers) {
        controllers[gamepads[i].index] = gamepads[i];
      } else {
        addgamepad(gamepads[i]);
      }
    }
  }
}


window.addEventListener("gamepadconnected", connecthandler);
window.addEventListener("gamepaddisconnected", disconnecthandler);

if (!haveEvents) {
  setInterval(scangamepads, 500);
}