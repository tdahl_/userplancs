#!/usr/bin/env python

from std_msgs.msg import *
from subprocess import *
import os, sys, rospy, subprocess, signal
from os.path import expanduser
from userplancs.srv import *

global bag_record
def createRequest():
    global refreshWebots, motionCreated
    rospy.init_node('node_server')

    rospy.Subscriber("/node_killed", String, killNode, queue_size=10 )
    rospy.Subscriber("/node_created", String, createNode, queue_size=10)
    rospy.Subscriber("/kill_rosbag", String, killRosbag, queue_size=10)

    refreshWebots = rospy.Publisher("refresh_webots", String, queue_size=10)
    motionCreated = rospy.Publisher("motion_launched", String, queue_size=10)

    createMotionFile = rospy.Service('/create_motion', UserplancsString, createMotion)
    grabMotion = rospy.Service('/grab_motion', UserplancsString, startGrabMotion)
    dropMotion = rospy.Service('/drop_motion', UserplancsString, startDropMotion)
    waveMotion = rospy.Service('/wave_motion', UserplancsString, startWaveMotion)
    userMotion = rospy.Service('/user_motion', UserplancsString, startUserMotion)
    pre_recorded_trigger = rospy.Service('/prerecorded_trigger', UserplancsString, startPrerecordedTrigger)

    topics = rospy.Service('/get_topics', UserplancsString, sendTopics)
    sysnodes = rospy.Service('/system_get_nodes', UserplancsString, sendSystemNodes)
    usernodes = rospy.Service('/user_get_nodes', UserplancsString, sendUserNodes)
    recorder = rospy.Service('/rosbag_recorder', UserplancsString, startRecording)
    killer = rospy.Service('/userplancs_killer', UserplancsString, killUserNode)

    rospy.spin()

# A service request method which creates and saves a motion file. 
# A message is published to record the creation in the ROSbag.
def createMotion(req):
    global motionCreated
    home = expanduser("~")
    file = open(home + '/workspace/src/userplancs/scripts/motions/userMotion.motion', 'w')
    file.write(req.request)
    file.close()
    motionCreated.publish("")
    return UserplancsStringResponse("")

def startPrerecordedTrigger(req):
    home = expanduser("~")
    call(["python", home + '/workspace/src/userplancs/scripts/pre-recorded_trigger.py'])
    return UserplancsStringResponse("")

# A service request to run the saved grab motion
def startGrabMotion(req):
    call(["rosrun","plancs","motionParse.py","userplancs_box_grab.motion"])
    return UserplancsStringResponse("")

# A service request to run the saved drop motion
def startDropMotion(req):
    call(["rosrun","plancs","motionParse.py","userplancs_box_drop.motion"])
    return UserplancsStringResponse("")

# A service request to run the saved wave motion
def startWaveMotion(req):
    call(["rosrun","plancs","motionParse.py","naoWave.motion"])
    return UserplancsStringResponse("")

# A service request to run the motion file recorded and saved by the user
def startUserMotion(req):
    call(["rosrun","plancs","motionParse.py", "userMotion.motion"])
    return UserplancsStringResponse("")

# A service request to start recording all the required topics in the ROSbag
# It will also stop the recording when the service request contains the message 'kill'
def startRecording(req):
    global bag_record, refreshWebots
    if req.request != "stop":
        home = expanduser("~")
        if not os.path.exists(home + "/bagfiles/" + req.request + "/"):
            os.makedirs(home +"/bagfiles/" + req.request + "/")
        array = []
        array.append("rosbag record")
        array.append("nao_top_camera")
        array.append("userplancs_newsense")
        array.append("node_created")
        array.append("node_killed")
        array.append("joy_to_nao_node/plancs_inhibit")
        array.append("userplancs_newsense/plancs_inhibit")
        array.append("start_end_experiment")
        array.append("controller")
        array.append("aligning")
        array.append("grasping")
        array.append("box_lifted")
        array.append("box_placed_down")
        array.append("box_dropped")
        array.append("motion_launched")
        array.append("motion_deleted")
        command = " ".join(array)
        dir_save_bagfile = home + "/bagfiles/" + req.request + "/"
        bag_record = Popen(command, stdin=PIPE, shell=True, cwd=dir_save_bagfile)
        return UserplancsStringResponse("")
    elif req.request == "stop":
        nodes = check_output(["rosnode", "list"])
        nodesSplit = nodes.split("\n")
        savedNodes = []
        for node in nodesSplit:
            if node.startswith("/record"):
                savedNodes.append(node)
        if len(savedNodes) > 0:
            call(["rosnode", "kill", savedNodes[0]])
        rospy.sleep(5)
        refreshWebots.publish("refresh")
        return UserplancsStringResponse("")

# Once a meesage has been received on this topic it will stop recording 
# in the ROSbag. This is called from the supervisor node when it stops the experiment.
def killRosbag(data):
    nodes = check_output(["rosnode", "list"])
    print nodes
    nodesSplit = nodes.split("\n")
    savedNodes = []
    for node in nodesSplit:
        if node.startswith("/record"):
            savedNodes.append(node)
    if len(savedNodes) > 0:
        call(["rosnode", "kill", savedNodes[0]])

# A service request to kill the user created ROS node.
def killUserNode(req):
    call(["rosnode", "kill", req.request])
    return UserplancsStringResponse("True")

# A topic to kill a node.
def killNode(data):
    call(["rosnode","kill",data.data])

# A function which accepts python code as a string and creates and launches it 
# as a ROS node.
def createNode(data):
    proc = Popen(["python"], stdin=PIPE)
    proc.communicate(input=data.data)
    return UserplancsStringResponse("True")

# Requests all Ros node names and sends the node names that contain plancs.
# These are nodes that are created by the programmer
def sendSystemNodes(req):
    savedNodes = getNodes(req, "/plancs")
    rospy.loginfo("Sending all node names that contain plancs.")
    return UserplancsStringResponse(savedNodes)

# Requests all Ros node names and sends the node names that contain userplancs.
# These are nodes that are created by the user
def sendUserNodes(req):
    savedNodes = getNodes(req, "/userplancs")
    rospy.loginfo("Sending all node names that contain userplancs.")
    return UserplancsStringResponse(savedNodes)

# A helper function which gets and returns all nodes that contain the name given
# as a parameter.
def getNodes(req ,whichNodes):
    lis = []
    lis = req.request.split(' ')
    nodes = check_output([lis[0].strip(), lis[1].strip()])
    nodesSplit = nodes.split("\n")
    savedNodes = []
    for node in nodesSplit:
        if node.startswith(whichNodes):
            savedNodes.append(node)

    nodes = "\n".join(savedNodes)
    return nodes

# A service request that gets the topics that are published by a ROS node
# and the corresponding message type and returns it to the webpage.
def sendTopics(req):
    lis = []
    lis = req.request.split(' ')
    info = check_output([lis[0].strip(), lis[1].strip(), lis[2].strip()])
    rospy.loginfo("Sending all publications from node '" + lis[2].strip() + "'")

    infoSplit = info.split("\n")
    topicList = []

    for x in range(3, len(infoSplit)):
        if infoSplit[x].strip() == "Subscriptions: None" or infoSplit[x].strip() == "Subscriptions:":
            break
        else:
            if not infoSplit[x] == "\n":
                topicList.append(infoSplit[x])  

    topics = []
    for i in range(len(topicList)):
        topics.append(topicList[i].split(" "))
      
    newTopics = []
    for i in range(len(topics)):
        if len(topics[i]) > 0:
            newTopics.append([])
        for j in range(len(topics[i])):
            if topics[i][j] == "" or topics[i][j] == "*":
                continue
            else:
                newTopics[i].append(topics[i][j])

    topics = []
    for x in range(len(newTopics)):
        if len(newTopics[x]) > 0:
            newTopics[x][1] = newTopics[x][1].strip("[").strip("]")
            topics.append(newTopics[x][0])
            topics.append(newTopics[x][1])


    if "/rosout" in topics:
        topics.remove("/rosout")
    if "rosgraph_msgs/Log" in topics:
        topics.remove("rosgraph_msgs/Log")

    final = ",".join(topics)

    return UserplancsStringResponse(final)

if __name__ == '__main__':
    createRequest()